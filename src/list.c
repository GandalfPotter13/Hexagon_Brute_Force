#include <stdio.h>
#include <time.h>

#define ASIZE 6

int array[ASIZE] = {1,2,3,4,5,6};//,7,8,9,10,11,12,13,14,15,16,17,18,19};

void permute(int l){
  int tmp;
  if(l==ASIZE-1){
    for(int i=0; i<ASIZE; ++i){
      printf("%d_",array[i]);
    } putchar('\n');
  }
  for(int i=l; i<ASIZE; ++i){
    tmp = array[i];
    array[i] = array[l];
    array[l] = tmp;
    permute(l+1);
    tmp = array[i];
    array[i] = array[l];
    array[l] = tmp;
  }
}

int main(){
  clock_t t;
  t=clock();

  permute(0);
  
  printf("completed in %f ms",(double)(clock()-t)/(CLOCKS_PER_SEC/1000));
}
