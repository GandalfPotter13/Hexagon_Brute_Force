// solution.c

#include <stdio.h>
#include <stdlib.h>

#define HEX_MAX 18
#define HEX_GOAL 38

int* createHexagon(){
  int* hexagon = malloc(sizeof(int)*HEX_MAX);
  if(hexagon == NULL){
    printf("buy more ram lol\n");
    exit(1);
  }
  for(int i=1; i<=HEX_MAX; ++i){
    hexagon[i-1] = i;
  }
  return hexagon;
}

int isSolution(int* h){
  if(HEX_MAX!=18||HEX_GOAL!=38)
    return 0;
  if(h[0]+h[1]+h[2]==HEX_GOAL
     && h[0]+h[3]+h[7]==HEX_GOAL  //3
     && h[2]+h[6]+h[11]==HEX_GOAL
     && h[7]+h[12]+h[16]==HEX_GOAL
     && h[11]+h[15]+h[18]==HEX_GOAL
     && h[16]+h[17]+h[18]==HEX_GOAL
     && h[3]+h[4]+h[5]+h[6]==HEX_GOAL  //4
     && h[12]+h[13]+h[14]+h[15]==HEX_GOAL
     && h[3]+h[8]+h[15]+h[17]==HEX_GOAL
     && h[1]+h[5]+h[10]+h[15]==HEX_GOAL
     && h[6]+h[10]+h[14]+h[17]==HEX_GOAL
     && h[1]+h[4]+h[8]+h[12]==HEX_GOAL
     && h[7]+h[8]+h[9]+h[10]+h[11]==HEX_GOAL  //5
     && h[0]+h[4]+h[9]+h[14]+h[18]==HEX_GOAL
     && h[2]+h[5]+h[9]+h[13]+h[16]==HEX_GOAL
     )
    return 1;
  return 0;
}

void printHexagon(int* h){
  printf(
	 "   == HEXAGON ==\n"
	 "    %d, %d, %d,\n"
	 "  %d, %d, %d, %d,\n"
	 "%d, %d, %d, %d, %d,\n"
	 "  %d, %d, %d, %d,\n"
	 "    %d, %d, %d\n\n",
	 h[0],h[1],h[2],h[3],h[4],h[5],h[6],h[7],h[8],h[9],h[10],h[11],h[12],h[13],h[14],h[15],h[16],h[17],h[18]);
}

void solveHexagon(int* h, int l, int* count){
  int tmp;
  if(l==HEX_MAX-1){
    printf("hexagon #%d\n",*count);
    ++*count;
    if(isSolution(h)){
      printf("found solution!\n");
      printHexagon(h);
    }
    return;
  }
  for(int i=l; i<HEX_MAX; ++i){
    tmp = h[i];
    h[i] = h[l];
    h[l] = tmp;
    solveHexagon(h, l+1, count);
    tmp = h[i];
    h[i] = h[l];
    h[l] = tmp;
  }
}

int main(){
  int* my_hexagon = createHexagon();
  int count = 1;
  solveHexagon(my_hexagon,0,&count);
  free(my_hexagon);
  
  return 0;
}
